// Notas
#define NOTE_C5  523
#define NOTE_D5  587
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_G5  784

const int pinBuzzer = 9;
int compasso = 900;
 
// Jingle Bells
 
int melody[] = {
  NOTE_E5, NOTE_E5, NOTE_E5,
  NOTE_E5, NOTE_E5, NOTE_E5,
  NOTE_E5, NOTE_G5, NOTE_C5, NOTE_D5,
  NOTE_E5,
  NOTE_F5, NOTE_F5, NOTE_F5, NOTE_F5,
  NOTE_F5, NOTE_E5, NOTE_E5, NOTE_E5, NOTE_E5,
  NOTE_E5, NOTE_D5, NOTE_D5, NOTE_E5,
  NOTE_D5, NOTE_G5
};
 
int tempo[] = {
  8, 8, 4,
  8, 8, 4,
  8, 8, 8, 8,
  2,
  8, 8, 8, 8,
  8, 8, 8, 16, 16,
  8, 8, 8, 8,
  4, 4
};

int botao = 0;

int som = 0;

void setup(void)  { 
  pinMode(pinBuzzer, OUTPUT); // Buzzer
  pinMode(12, OUTPUT); // Led indicador nota NOTE_E5 
  pinMode(8, OUTPUT); // Led indicador nota NOTE_G5
  pinMode(7, OUTPUT); // Led indicador nota NOTE_C5
  pinMode(4, OUTPUT); // Led indicador nota NOTE_F5
  pinMode(2, OUTPUT); // Led indicador nota NOTE_D5
  pinMode(13, INPUT_PULLUP); // botão

  Serial.begin(9600);
}


void loop() {
  botao = digitalRead(13);
  if (botao == 0) {
    sing(1);
  } 
}


void sing(int s) {

  som = s;
  if (som == 1) {
    Serial.println(" 'Jingle Bells'");
    int size = sizeof(melody) / sizeof(int);
    for (int estaNota = 0; estaNota < size; estaNota++) {
       
      // Tomamos 1 segundo divido pelo tipo da nota para calcular a duração
      int notaTempo = compasso / tempo[estaNota];
 
      buzz(pinBuzzer, melody[estaNota],notaTempo);
 
      // para distinguir melhor as notas, defimos um tempo mínimo entre ela
      int pausaNotas = notaTempo * 1.30;
      delay(pausaNotas);
 
      // interrompe a melodia:
      buzz(pinBuzzer, 0, notaTempo);
    }
  }
}

void buzz(int pin , int frequencia, float dur) {
   
  if(frequencia == NOTE_E5) {
  	digitalWrite(12, HIGH);
  } 
  
  if (frequencia == NOTE_G5) {
    digitalWrite(8, HIGH);
  }
  
  if (frequencia == NOTE_C5){
    digitalWrite(7, HIGH);
  }
  
  
   if (frequencia == NOTE_F5){
    digitalWrite(4, HIGH);
  }
  
   if (frequencia == NOTE_D5){
    digitalWrite(2, HIGH);
  }
  
 
    
  int delayValor = 1000000 / frequencia / 2; // calcula o delay entre transições
  
 
  int numCiclos = frequencia * dur / 1000; // calcula o número de ciclos adequado
  
 
  for (int i = 0; i < numCiclos; i++) { // para definir a duração da nota
    digitalWrite(pin, HIGH); // buzzer em HIGH para emprurrar o diafragama do buzzer
    delayMicroseconds(delayValor); // espera o valor calculado do delay
    digitalWrite(pin, LOW); // buzzer em LOW para puxar o diafragama do buzzer
    delayMicroseconds(delayValor); // espera o valor calculado do delay
    }
 
   if(frequencia == NOTE_E5) {
  	digitalWrite(12, LOW);
   }
  
  if(frequencia == NOTE_G5) {
    digitalWrite(8, LOW);
  }
  
   if(frequencia == NOTE_C5) {
    digitalWrite(7, LOW);
  }
  
   if(frequencia == NOTE_F5) {
    digitalWrite(4, LOW);
  }
  
   if(frequencia == NOTE_D5) {
    digitalWrite(2, LOW);
  }
}